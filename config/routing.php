<?php

// Routing - Custom URL

$route = array(
			
			"pdf" => "a-pdf-generator.php",
			"manual" => "a-home-brand.php",
			"bak" => "a-homeBAK.php",
			"bar" => "a-rand.php",
			"file" => "a-file-detail.php",
			"search" => "a-search.php",
			"download" => "download.php"
		);
		
$route_adc = "dl"; // Ad-Center Routing