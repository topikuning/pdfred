<?php
$cURI = explode('/', $_SERVER['REQUEST_URI']);
$title = str_replace('-',' ',$cURI[2]);
$referrer = $_SERVER['HTTP_REFERER'];
?>


<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">

<title>Downloading File <?=ucwords($title);?></title>
<meta name="robot" content="noindex,nofollow">

<link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<script type="text/javascript" src="/js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<link rel='stylesheet' id='open-sans-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=3.8' type='text/css' media='all' />	

</head>
<body>
<script language="javascript">
  var max_time = 5;
  var cinterval;
   
  function countdown_timer(){
  // decrease timer
  max_time--;
  document.getElementById('count').innerHTML = max_time;
  if(max_time == 0){
  clearInterval(cinterval);
  }
  }
  // 1,000 means 1 second.
  cinterval = setInterval('countdown_timer()', 1000);
</script>

<div class="container">
  <div class="alert alert-success" style="font-size:20px; margin-top:150px;" role="alert">
  <center><i class="fa fa-download"></i> &nbsp;
  <strong style="font-size:20px;">Congratulation!</strong>
  "<?=strtoupper($title);?>" document to be ready in <span id="count" style="font-size:20px;">5</span> seconds.</center>
  </div>
  
</div>

<meta http-equiv="refresh" content="5; url=http://ads.ad-center.com/offer?prod=2&ref=kodeads_user&q=<?=$title?>">

</body>
</html>
