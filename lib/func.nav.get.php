<?php

class pageNav{

	var $int_max_rows;
	var $string_table;
	var $string_field2select;
	var $int_cur_page;
	var $cur_sql_pos;
	var $string_cur_uri;
	var $int_tot_rows;
	var $int_tot_page;
	var $string_class;
	var $string_prev_word;
	var $string_next_word;
	var $string_result;
	var $string_to;
	var $string_of;

	function pageNav($int_max_rows, $string_table, $string_field2select="*", $string_table_id="id"){
		$this->string_class="Spec";
		$this->string_prev_word='<i class="fa fa-chevron-left"></i>';
		$this->string_next_word='<i class="fa fa-chevron-right"></i>';
		$this->string_result='<i class="fa fa-eye"></i>';
		$this->string_to="-";
		$this->string_of=":";
		$this->int_max_rows = $int_max_rows;
		$this->string_table = $string_table;
		$this->string_field2select = $string_field2select;

		if(isset($_GET['page'])){
			if($_GET['page']!=""){
				$this->int_cur_page = $_GET['page'];
			}
		}else 
			$this->int_cur_page = 1;
		
		$this->cur_sql_pos = ($this->int_cur_page-1)*$int_max_rows;
		
		$sql_tot_rows = "$string_table";
		$result_tot_rows = mysql_query($sql_tot_rows);
		$tot_rows = mysql_num_rows($result_tot_rows);
		$this->int_tot_rows = $tot_rows;
		
		$this->int_tot_page = ceil($this->int_tot_rows / $int_max_rows);

		$tmpStr = $this->int_cur_page;
		$jmlangka = strlen($tmpStr);

		$cur_page = (preg_match("/\?/", $_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI']."&" : $_SERVER['REQUEST_URI']."?";
		
		//$ccfixuri = preg_replace("#page=\d{".$jmlangka."}&{0,1}#", "page=".$jmlangka, $cur_page);
		
		$this->string_cur_uri = preg_replace("#&{0,1}page=\d{".$jmlangka."}#", "", $cur_page);
		
	}

	function setPrevNextWord($string_prev_word, $string_next_word ){
		$this->string_prev_word = $string_prev_word;
		$this->string_next_word = $string_next_word;
	}
	function setClass($string_class){
		$this->string_class = $string_class;
	}	
	function setStatusWord($string_result, $string_to, $string_of ){
		$this->string_result = $string_result;
		$this->string_to = $string_to;
		$this->string_of = $string_of;		
	}	
	
	function getPrev(){
		if($this->int_cur_page != 1){
			if($this->int_cur_page == 2){
				$ifixgetq = str_replace('?&', '', $this->string_cur_uri);
				$string_prev = '<li><a href="'.  rtrim($ifixgetq, "&") .'" title="Previous">'. $this->string_prev_word .'</a></li>';
				return $string_prev;
			}else{
				$int_prev_page = $this->int_cur_page-1;
				$string_prev = '<li><a href="'. str_replace('?&', '?', $this->string_cur_uri) .'page='. $int_prev_page .'" title="Previous">'. $this->string_prev_word .'</a></li>';
				return $string_prev;
			}
		}else return("");
	}
	
	function getNext(){
		if($this->int_cur_page!=$this->int_tot_page && $this->int_tot_page!=0){
			$int_next_page = $this->int_cur_page+1;
			$string_next = '<li><a href="'. str_replace('?&', '?', $this->string_cur_uri) .'page='. $int_next_page .'" title="Next">'. $this->string_next_word .'</a></li>';
			return $string_next;
		}else return("");
	}
	
	function getArrNav(){
		$arr_nav = array();
		$lastpage = $this->int_tot_page-5;
		for ($i = 0; $i < $this->int_tot_page; $i++){
			$j = $i+1;
			if($j == 1 && $this->int_cur_page != 1){
				$ifixgetq = str_replace('?&', '', $this->string_cur_uri);
				$arr_nav[$i] = '<li><a href="'. rtrim($ifixgetq, "&") .'" title="Page '. $j .'">'. $j .'</a></li>';
			}else{
				if($this->int_cur_page > 10){
					$pn = $this->int_cur_page - 6;
					if($this->int_cur_page + 6 < $this->int_tot_page){
						$pm = $this->int_cur_page + 6;
					}else{
						$pm = $this->int_tot_page;
					}
					if($this->int_cur_page == $j){
						// Page Sekarang
						$arr_nav[$i] = '<li class="active"><a class="active" href="'. str_replace('?&', '?', $this->string_cur_uri) .'page='. $j .'" title="Page '. $j .'">'. $j .'</a></li>';
					}elseif($j > $pn && $j <= $pm){ 
						$arr_nav[$i] = '<li><a href="'. str_replace('?&', '?', $this->string_cur_uri) .'page='. $j .'" title="Page '. $j .'">'. $j .'</a></li>';
					}
				}elseif($this->int_cur_page < 10 && $this->int_tot_page > 10){
					if($this->int_cur_page == $j){
						// Page Sekarang
						$arr_nav[$i] = '<li class="active"><a class="active" href="'. str_replace('?&', '?', $this->string_cur_uri) .'page='. $j .'" title="Page '. $j .'">'. $j .'</a></li>';
					}elseif($j <= 10){ 
						$arr_nav[$i] = '<li><a href="'. str_replace('?&', '?', $this->string_cur_uri) .'page='. $j .'" title="Page '. $j .'">'. $j .'</a></li>';
					}
				}elseif($this->int_cur_page == 10 && $this->int_tot_page > 10){
					$pt = $this->int_cur_page - 7;
					if($this->int_cur_page == $j){
						// Page Sekarang
						$arr_nav[$i] = '<li class="active"><a class="active" href="'. str_replace('?&', '?', $this->string_cur_uri) .'page='. $j .'" title="Page '. $j .'">'. $j .'</a></li>';
					}elseif($j > $pt && $j < 16){ 
						$arr_nav[$i] = '<li><a href="'. str_replace('?&', '?', $this->string_cur_uri) .'page='. $j .'" title="Page '. $j .'">'. $j .'</a></li>';
					}
				}else{
					if($this->int_cur_page == $j){
						// Page Sekarang
						$arr_nav[$i] = '<li class="active"><a class="active" href="'. str_replace('?&', '?', $this->string_cur_uri) .'page='. $j .'" title="Page '. $j .'">'. $j .'</a></li>';
					}else{ 
						if($this->int_cur_page == 2 && $j == 1){
							// Page 2
							$arr_nav[$i] = '<li><a href="'. str_replace('?&', '', $this->string_cur_uri) .'" title="Page '. $j .'">'. $j .'</a></li>';
						}else{
							// Page Lain
							$arr_nav[$i] = '<li><a href="'. str_replace('?&', '?', $this->string_cur_uri) .'page='. $j .'" title="Page '. $j .'">'. $j .'</a></li>';
						}
					}
				}
			}
		}
		return $arr_nav;
	}
	
	function getNavStatus(){		
		$arr_status['begin'] = $this->cur_sql_pos;
		$num_view_rows = $this->cur_sql_pos+$this->int_max_rows;
		$arr_status['end'] = ($num_view_rows < $this->int_tot_rows) ? $num_view_rows : $this->int_tot_rows;
		$arr_status['total'] =  $this->int_tot_rows;
		return $arr_status;
	}
	
	function printNav(){
	$lastpage = $this->int_tot_page-5;
		if($this->int_tot_page > 1){
			$prev = $this->getPrev();
			$next = $this->getNext();
			$arr_nav = $this->getArrNav();
			foreach ($arr_nav as $key=>$val)
				$print_nav .= $val ." ";
			if($this->int_tot_page > 8){
				if( $this->int_cur_page > 5 && $this->int_cur_page <= $lastpage ){
					$print_nav = $prev . $print_nav . $next;
				}elseif( $this->int_cur_page <= 5 ){
					$print_nav = $prev ." ". $print_nav . $next;
				}elseif( $this->int_cur_page > $lastpage ){
					$print_nav = $prev . $print_nav . $next;
				}
			}else{
				$print_nav = $prev ." ". $print_nav ." ". $next;
			}
			if($this->int_tot_page > 10 && $this->int_cur_page < $this->int_tot_page){
				$print_nav = $print_nav .' <li><a href="'. str_replace('?&', '?', $this->string_cur_uri) .'page='.$this->int_tot_page .'"><i class="fa fa-step-forward"></i></a></li>';
			}
			if($this->int_cur_page > 10){
				$print_nav = '<li><a href="'. str_replace('?&', '', $this->string_cur_uri) .'"><i class="fa fa-step-backward"></i></a></li> '. $print_nav;
			}
			echo '<ul class="pagination">'.$print_nav.'</ul>';
		}
	}
	
	function printStatus(){
		$status = $this->getNavStatus();
		echo $this->string_result ." ". $status['begin'];
		echo " ". $this->string_to ." ". $status['end'];
		echo " ". $this->string_of ." ". number_format($status['total']);
	}
	
	function getSQLresult(){
		$data_sql = $this->string_table ." LIMIT ". $this->cur_sql_pos .", ". $this->int_max_rows;
		$data_result = mysql_query($data_sql);
		return $data_result;
	}
}
?>