<?php

function fixTerm($str){
	$rest = str_replace('https', "", trim($str));
	$rest = str_replace('http', "", trim($rest));
	$rest = str_replace('www', "", trim($rest));
	$rest = str_replace('+', "", trim($rest));
	$rest = slugThis($rest);
	$rest = str_replace("-", " ", $rest);
	$rest = str_replace('filetype pdf', "", $rest);
	$rest = str_replace('filetype', "", $rest);
	$rest = str_replace('pdf', "", $rest);
	return trim($rest);
}

function slugThis($str){
	$slg = strtolower(str_replace(' ', "-", trim($str)));
	$slg = str_replace('%20', "-", $slg);
	$slg = str_replace('+', "-plus-", $slg);
	$slg = str_replace('_', "-", $slg);
	$slg = preg_replace('|[^a-z0-9-]|i', "", $slg);
	$slg = str_replace('-plus-', "+", $slg);
	$slg = str_replace('--', "-", $slg);
	$slg = str_replace('--', "-", $slg);
	return $slg;
}

function fixTitle($str){
	$tit = strtoupper($str);
	
	$tit = str_replace('æ', "&#198;", $tit);
	$tit = str_replace('ä', "&#196;", $tit);
	$tit = str_replace('ø', "&#216;", $tit);
	$tit = str_replace('ö', "&#214;", $tit);
	$tit = str_replace('ü', "&#220;", $tit);
	$tit = str_replace('à', "&#192;", $tit);
	$tit = str_replace('á', "&#193;", $tit);
	$tit = str_replace('ê', "&#202;", $tit);
	$tit = str_replace('é', "&#201;", $tit);
	$tit = str_replace('œ', "&#338;", $tit);
	$tit = str_replace('î', "&#206;", $tit);
	$tit = str_replace('å', "&#197;", $tit);
	$tit = str_replace('ż', "&#379;", $tit);
	$tit = str_replace('ń', "&#323;", $tit);
	$tit = str_replace('ł', "&#321;", $tit);
	$tit = str_replace('ó', "&#211;", $tit);
	$tit = str_replace('ą', "&#260;", $tit);
	$tit = str_replace('ę', "&#280;", $tit);
	$tit = str_replace('ć', "&#262;", $tit);
	$tit = str_replace('ś', "&#346;", $tit);
	$tit = str_replace('ğ', "&#286;", $tit);
	$tit = str_replace('ş', "&#350;", $tit);
	$tit = str_replace('ç', "&#199;", $tit);
	
	$tit = str_replace('&#246;', "&#214;", $tit);
	$tit = str_replace('&#230;', "&#198;", $tit);
	$tit = str_replace('&#248;', "&#216;", $tit);
	$tit = str_replace('&#228;', "&#196;", $tit);
	$tit = str_replace('&#252;', "&#220;", $tit);
	$tit = str_replace('&#224;', "&#192;", $tit);
	$tit = str_replace('&#225;', "&#193;", $tit);
	$tit = str_replace('&#234;', "&#202;", $tit);
	$tit = str_replace('&#233;', "&#201;", $tit);
	$tit = str_replace('&#339;', "&#338;", $tit);
	$tit = str_replace('&#238;', "&#206;", $tit);
	$tit = str_replace('&#229;', "&#197;", $tit);
	$tit = str_replace('&#380;', "&#379;", $tit);
	$tit = str_replace('&#324;', "&#323;", $tit);
	$tit = str_replace('&#322;', "&#321;", $tit);
	$tit = str_replace('&#243;', "&#211;", $tit);
	$tit = str_replace('&#261;', "&#260;", $tit);
	$tit = str_replace('&#281;', "&#280;", $tit);
	$tit = str_replace('&#263;', "&#262;", $tit);
	$tit = str_replace('&#347;', "&#346;", $tit);
	$tit = str_replace('&#287;', "&#286;", $tit);
	$tit = str_replace('&#351;', "&#350;", $tit);
	$tit = str_replace('&#231;', "&#199;", $tit);
	
	$tit = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
		'|[\x00-\x7F][\x80-\xBF]+'.
		'|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
		'|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
		'|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
		'', $tit );
	$tit = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
		'|\xED[\xA0-\xBF][\x80-\xBF]/S','', $tit );
	return $tit;
}

// DATE
$monthshortint = array(1 => "Jan",2 => "Feb",3 => "Mar",4 => "Apr",5 => "May",6 => "Jun",7 => "Jul",8 => "Aug",9 => "Sep",10 => "Oct",11 => "Nov",12 => "Dec");
$monthlongint = array(1 => "January",2 => "February",3 => "March",4 => "April",5 => "May",6 => "June",7 => "July",8 => "August",9 => "September",10 => "October",11 => "November",12 => "December");
$monthshortind = array(1 => "Jan",2 => "Peb",3 => "Mar",4 => "Apr",5 => "Mei",6 => "Jun",7 => "Jul",8 => "Agu",9 => "Sep",10 => "Okt",11 => "Nop",12 => "Des");
$monthlongind = array(1 => "Januari",2 => "Pebruari",3 => "Maret",4 => "April",5 => "Mei",6 => "Juni",7 => "Juli",8 => "Agustus",9 => "September",10 => "Oktober",11 => "Nopember",12 => "Desember");

function dateShort($ddddmmyy, $strDivider = "-"){
	global $monthshortint;
	$buf = explode($strDivider, $ddddmmyy);return $monthshortint[$buf[1]]." ".$buf[2].", ".$buf[0];
}
function dateLong($ddddmmyy, $strDivider = "-"){
	global $monthlongint;
	$buf = explode($strDivider, $ddddmmyy);return $monthlongint[$buf[1]]." ".$buf[2].", ".$buf[0];
}
function dateIndoShort($ddddmmyy, $strDivider = "-"){
	global $monthshortind;
	$buf = explode($strDivider, $ddddmmyy);return $buf[2]." ".$monthshortind[$buf[1]].", ".$buf[0];
}
function dateIndoLong($ddddmmyy, $strDivider = "-"){
	global $monthlongind;
	$buf = explode($strDivider, $ddddmmyy);return $buf[2]." ".$monthlongind[$buf[1]].", ".$buf[0];
}

// ENCODE DECODE BASE62
function b62encode($val, $base=62, $chars='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
    // can't handle numbers larger than 2^31-1 = 2147483647
    $str = '';
    do {
        $i = $val % $base;
        $str = $chars[$i] . $str;
        $val = ($val - $i) / $base;
    } while($val > 0);
    return $str;
}

function b62decode($str, $base=62, $chars='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
    $len = strlen($str);
    $val = 0;
    $arr = array_flip(str_split($chars));
    for($i = 0; $i < $len; ++$i) {
        $val += $arr[$str[$i]] * pow($base, $len-$i-1);
    }
    return $val;
}

function readfile_chunked($filename, $retbytes=true) {
   $chunksize = 1*(1024*1024); // how many bytes per chunk
   $buffer = '';
   $cnt =0;
   $handle = fopen($filename, 'rb');
   if ($handle === false) {
       return false;
   }
   while (!feof($handle)) {
       //$buffer = fread($handle, $chunksize);
       //echo $buffer;
	   print(fread($handle, 1024*8));
       ob_flush();
       flush();
       //if ($retbytes) {
           //$cnt += strlen($buffer);
       //}
   }
       $status = fclose($handle);
   if ($retbytes && $status) {
       return $cnt; // return num. bytes delivered like readfile() does.
   }
   return $status;

} 

function downloadFile( $fullPath ){

  // Must be fresh start
  if( headers_sent() )
    die('Headers Sent');

  // Required for some browsers
  if(ini_get('zlib.output_compression'))
    ini_set('zlib.output_compression', 'Off');

  // File Exists?
  if( file_exists($fullPath) ){
   
    // Parse Info / Get Extension
    $fsize = filesize($fullPath);
    $path_parts = pathinfo($fullPath);
    $ext = strtolower($path_parts["extension"]);
   
    // Determine Content Type
    switch ($ext) {
      case "pdf": $ctype="application/pdf"; break;
      case "exe": $ctype="application/octet-stream"; break;
      case "zip": $ctype="application/zip"; break;
      case "doc": $ctype="application/msword"; break;
      case "xls": $ctype="application/vnd.ms-excel"; break;
      case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
      case "gif": $ctype="image/gif"; break;
      case "png": $ctype="image/png"; break;
      case "jpeg":
      case "jpg": $ctype="image/jpg"; break;
      default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false); // required for certain browsers
    header("Content-Type: $ctype");
    header("Content-Disposition: attachment; filename=\"".basename($fullPath)."\";" );
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: ".$fsize);
    ob_clean();
    flush();
    readfile_chunked($fullPath);
  } else
    die('File Not Found');
}
