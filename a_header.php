<?php require_once('cache.php'); ?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-en" lang="en-en" >
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?=$tit_hdr?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name='title' content='<?=strtolower($titlebos);?> on <?=strtolower($_DOM);?>' />
<meta name="description" content='<?=$description?>'>
<meta name='robots' content='<?=$robots?>' />
<meta name="author" content='<?=$_DOM?>'>
<link rel="stylesheet" type="text/css" href="/css/style.css" />
</head>
<body>

<div id="header">
    <div id="headerInside">
	    <strong><?=strtoupper($_DOM);?> COLLECTIONS</strong>
    </div>
</div>
