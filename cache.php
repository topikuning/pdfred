<?php
define('CACHE_PATH', dirname(__FILE__) . '/tmp/cache_');
define('CACHE_TIME', 48);

function cache_file()
{
    return CACHE_PATH . md5($_SERVER['REQUEST_URI']);
}

function cache_display()
{
    $file = cache_file();

    if(!file_exists($file)) return;
    if(filemtime($file) < time() - CACHE_TIME * 3600) return;
    $cache = file_get_contents($file);
    echo minify_output($cache);
    exit;
}

function cache_page($content)
{
    if(false !== ($f = @fopen(cache_file(), 'w'))) {
        fwrite($f, $content);
        fclose($f);
    }
    return $content;
}

function minify_output($buffer)
{
    $search = array(
    '/\>[^\S ]+/s',
    '/[^\S ]+\</s',
    '/(\s)+/s'
    );
    $replace = array(
    '>',
    '<',
    '\\1'
    );
    if (preg_match("/\<html/i",$buffer) == 1 && preg_match("/\<\/html\>/i",$buffer) == 1) {
    $buffer = preg_replace($search, $replace, $buffer);
    }
    return $buffer;
}

$referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : false;
$parsedReferer = parse_url($referer);

// Bypass cache if referer is from Bing, Google, Yahoo
if (!preg_match('/(bing|google|yahoo)\./i', $parsedReferer['host']) && !isset($_POST['goSearch'])) {
    cache_display();
    ob_start('cache_page');
}