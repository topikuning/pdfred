<?php
// Doneeh 2014

require('config/config.php');

$connect = mysql_connect($_MYSQL_HOST, $_MYSQL_USER, $_MYSQL_PASS) or die(mysql_error());
@mysql_select_db($_MYSQL_DB);

ini_set('session.cookie_domain', '.'.$_DOM);
session_set_cookie_params(0, '/', '.'.$_DOM);
@session_start();

// --------------------------------------------------------------//

require_once('lib/func.doneeh.php');

//===============================================================//
$HOST = $_SERVER['HTTP_HOST'];
$cURI = explode('/', $_SERVER['REQUEST_URI']);
$tURI = count($cURI);
//===============================================================//

include('config/routing.php');

// --------------------------------------------------------------//

function href($file){
	global $route;
	return array_search($file, $route);
}

//===============================================================//

if(file_exists('error_log')){
	@unlink('error_log');
}
