<?php
require('config/config.php');
$xml_per_page = '49999';
$URI = $_SERVER["REQUEST_URI"];
$eURI = explode("/", $URI);
$xml_page = $eURI[2];

if ($xml_page == 'index.xml') {
	require_once('sitemap_index.php');
} else {
	require_once('sitemap_detail.php');
}
?>
