<?php 
$titlebos = 'Privacy Policy Page - '.$_SERVER['HTTP_HOST'].'';
$robots = 'noindex,nofollow';
include('a_header.php');
?>

<div id="content">

<p><?php echo strtoupper($_SERVER['HTTP_HOST']); ?> is committed to user privacy and/or our policy below and describes our principles in maintaining user trust and confidence and protecting your private data.</p>
          
          <p>While visiting <?php echo strtoupper($_SERVER['HTTP_HOST']); ?> you can be sure that our site doesn't collect and store any personal information, but from time to time we can collect the information you browser contains: your IP address, type of your browser, what URL you have used and when.</p> 
          <p>This information is used only with the purpose to improve the quality of our site and level of the services we offer. We want to admit that sometimes we can share the collected information with some third-parties, but just to help them to make our site even better.</p> 
          <p>For example, if there is a group of users looking for a specific book or clicking the same advertising proposition, we can collect and share this information with third-parties to create targeting advertising or to offer these users more similar books to read.
          </p><p>We strictly follow to all laws on personal information safety to protect our user's right and the safety of our users' data.</p>
</div>

<?php include('a_footer.php');?>